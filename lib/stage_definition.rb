# frozen_string_literal: true

require 'yaml'

require_relative './www_gitlab_com'

module StageDefinition
  module_function

  STAGE_DATA = WwwGitLabCom.stages
  GROUP_DATA = YAML.load_file(File.expand_path("#{__dir__}/../group-definition.yml")).freeze

  DEFAULT_ASSIGNEE_FIELDS = %w[
    pm
    engineering_manager
    backend_engineering_manager
    frontend_engineering_manager
    extra_assignees
  ].freeze
  ALL_ASSIGNEE_FIELDS = DEFAULT_ASSIGNEE_FIELDS + ['extra_mentions']

  STAGE_DATA.each do |name, definition|
    define_method("stage_#{name}") do |fields = nil|
      groups = definition['groups'].to_h do |group|
        [group, GROUP_DATA[group]]
      end.compact

      assignables = groups.each_value.reduce({}) do |acc, details|
        acc.merge(details.slice(*ALL_ASSIGNEE_FIELDS)) { |k, a, b| (a + b).uniq }
      end

      assignee_fields =
        if fields
          fields & DEFAULT_ASSIGNEE_FIELDS
        else
          DEFAULT_ASSIGNEE_FIELDS
        end

      result = {
        assignees:
          assignables.values_at(*assignee_fields).flatten.compact.uniq,
        labels:
          definition['labels'] || ["devops::#{name.tr('_', ' ')}"],
        groups: groups
      }

      if fields.nil? || fields.include?('extra_mentions')
        extra_mentions = assignables['extra_mentions']
        if extra_mentions
          result[:mentions] = (result[:assignees] + extra_mentions).uniq
        end
      end

      result
    end
  end
end
