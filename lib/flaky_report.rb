# frozen_string_literal: true

require 'gitlab'
require 'net/http'
require 'uri'
require 'cgi'
require 'tempfile'

require_relative '../triage/triage'
require_relative 'flaky_report_analyzer'

class FlakyReport
  COM_GITLAB_API = 'https://gitlab.com/api/v4'
  MAX_WAIT_TIME = 3600
  GITLAB_BOT_USERNAME = 'gitlab-bot'
  FLAKY_REPORT_JOB_NAME = 'update-tests-metadata'
  ARTIFACT_PATH = 'rspec_flaky/report-suite.json'
  ArtifactDownloadFailedError = Class.new(StandardError)
  FlakyReportBinDownloadFailedError = Class.new(StandardError)

  Gitlab.configure do |config|
    config.endpoint = COM_GITLAB_API
  end

  def self.latest_flaky_report(project_path:, token:, debug: ENV['DEBUG'])
    new(project_path: project_path, token: token, debug: debug)
    .latest_flaky_report
  end

  def initialize(project_path:, token:, debug: ENV['DEBUG'])
    raise ArgumentError, 'An API token is needed!' if token.nil?

    @project_path = project_path
    @token = token
    @debug = debug
    @client = Gitlab.client(endpoint: Triage::PRODUCTION_API_ENDPOINT, private_token: token)
  end

  def latest_flaky_report
    pipeline = latest_schedule_pipeline

    debug "Latest successful scheduled pipeline (#{pipeline.id}) found."

    with_flaky_report_job(pipeline) do |job|
      issue_content_from_job(job)
    end
  end

  private

  attr_reader :project_path, :token, :client

  def debug?
    !!@debug
  end

  def debug(text)
    return unless debug?

    puts "[DEBUG] #{text}"
  end

  def latest_schedule_pipeline
    client.pipelines(project_path, username: GITLAB_BOT_USERNAME, status: 'success', per_page: 1).first
  end

  def pipeline_jobs(pipeline)
    client.pipeline_jobs(project_path, pipeline.id, per_page: 100).auto_paginate do |job|
      yield job
    end
  end

  def project_job(job)
    client.job(project_path, job.id)
  end

  def with_flaky_report_job(pipeline)
    pipeline_jobs(pipeline) do |job|
      next unless job.name == FLAKY_REPORT_JOB_NAME

      debug "Flaky Reports job '#{job.name}' (#{job.id}) found (status: #{job.status})"

      start_time = Time.now
      loop do
        break if (Time.now - start_time) >= MAX_WAIT_TIME
        break if job.status == 'success'

        print '.'
        sleep 10

        job = project_job(job)
      end

      return yield job
    end
  end

  def issue_content_from_job(job)
    [].tap do |issue_content|
      with_latest_flaky_report(job) do |latest_flaky_report|
        issue_content << '### Top 20 flaky tests sorted by occurrences per file'
        issue_content << ("```\n" + FlakyReportAnalyzer.new(latest_flaky_report.path).print_report(group_by: :file, limit: 20) + "\n```")

        issue_content << '### Number of flaky tests per type'
        issue_content << ("```\n" + FlakyReportAnalyzer.new(latest_flaky_report.path).print_report(group_by: :type, limit: 20) + "\n```")

        issue_content << '### Flaky tests per day for the last 20 days'
        issue_content << ("```\n" + FlakyReportAnalyzer.new(latest_flaky_report.path).print_report(group_by: :date, limit: 20) + "\n```")

        issue_content << '### Top 20 flaky test examples sorted by number of reports'
        issue_content << ("```\n" + FlakyReportAnalyzer.new(latest_flaky_report.path).print_report(group_by: :example_id_reports, limit: 20) + "\n```")
      end
    end.join("\n\n")
  end

  def with_latest_flaky_report(job)
    artifact_uri = URI(COM_GITLAB_API + "/projects/#{CGI.escape(project_path)}/jobs/#{job.id}/artifacts/#{ARTIFACT_PATH}")

    Net::HTTP.start(artifact_uri.host, artifact_uri.port, use_ssl: true) do |http|
      request = Net::HTTP::Get.new(artifact_uri)
      request['PRIVATE-TOKEN'] = token

      http.request(request) do |response|
        case response
        when Net::HTTPSuccess then
          Tempfile.create('latest_flaky_report') do |f|
            f.write(response.body)
            f.flush # This is required when testing
            yield f
          end
        else
          raise ArtifactDownloadFailedError, response.value
        end
      end
    end
  end
end
