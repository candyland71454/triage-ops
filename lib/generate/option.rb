# frozen_string_literal: true

module Generate
  Option = Struct.new(:template, :only, :assign, keyword_init: true) do
    def self.parse(argv)
      require 'optparse'

      argv << '--help' if argv.empty?
      options = new

      OptionParser.new do |parser|
        parser.banner = "Usage: #{$PROGRAM_NAME} [options]\n\n"

        parser.on('-t', '--template TEMPLATE', String, 'Path to the template') do |value|
          options.template = value
        end

        parser.on('-o', '--only TEAMS', String, 'Comma delineated list of stages or groups') do |value|
          options.only = value.split(',')
        end

        parser.on('-a', '--assign ASSIGNEEES', String, 'Comma delineated list of assignee roles') do |value|
          options.assign = value.split(',')
        end

        parser.on('-h', '--help', 'Print help message') do
          $stdout.puts parser
          exit
        end
      end.parse!(argv)

      options
    end
  end
end
