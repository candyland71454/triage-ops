# frozen_string_literal: true

require_relative 'devops_labels'
require_relative 'team_member_select_helper'
require_relative 'constants/labels'

module UntriagedHelper
  include DevopsLabels::Context
  include TeamMemberSelectHelper

  TYPE_LABELS = Labels::TYPE_LABELS
  SUBTYPE_LABELS = Labels::SUBTYPE_LABELS
  SPECIAL_ISSUE_LABELS = Labels::SPECIAL_ISSUE_LABELS
  TYPE_IGNORE_LABEL = Labels::TYPE_IGNORE_LABEL

  def distribute_items(list_items, triagers)
    puts "Potential triagers: #{triagers.inspect}"

    return { "unknown" => list_items } if triagers.empty?

    triagers.shuffle
      .zip(list_items.each_slice((list_items.size.to_f / triagers.size).ceil))
      .sort { |(triager_a, _), (triager_b, _)| triager_a <=> triager_b }
      .to_h
      .compact
  end

  def distribute_and_display_items_per_triager(list_items, triagers)
    items_per_triagers = distribute_items(list_items, triagers)

    text = items_per_triagers.each_with_object([]) do |(triager, items), text|
      text << "#{triager}\n\n#{items.join("\n")}"
    end.join("\n\n")

    text + "\n/assign #{items_per_triagers.keys.join(' ')}"
  end

  def merge_requests_from_line_items(items)
    items.lines(chomp: true).map do |item|
      author, url, *labels = item.split(',')
      { author: author, url: url, labels: labels }
    end
  end

  def markdown_table_rows(merge_requests_by_author)
    merge_requests_by_author.map.with_index do |(author, merge_requests), index|
      "| #{index + 1}: #{author} | <ol>#{merge_requests.map { |mr| "<li>#{mr[:url]}: #{mr[:labels].join(' ')}</li>" }.join}</ol> |"
    end
  end

  def untriaged?
    !triaged?
  end

  def triaged?
    special_issue? || has_type_ignore_label? ||
      (
        has_type_label? &&
        (
          has_department_label? ||
          (has_stage_label? && has_group_label?) ||
          label_names.include?('group::not_owned')
        )
      )
  end

  def issue_labels_missing?
    !has_type_label? &&
      !has_subtype_label? &&
      !has_type_ignore_label? &&
      !special_issue?
  end

  def special_issue?
    special_issue_labels.any?
  end

  def has_type_label?
    !type_label.nil?
  end

  def has_subtype_label?
    !subtype_label.nil?
  end

  def special_issue_labels
    (label_names & SPECIAL_ISSUE_LABELS)
  end

  def type_label
    (label_names & TYPE_LABELS).first
  end

  def subtype_label
    (label_names & SUBTYPE_LABELS).first
  end

  def has_type_ignore_label?
    label_names.include? TYPE_IGNORE_LABEL
  end
end
