# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/label_inference'
require_relative '../../triage/triage/event'

RSpec.describe Triage::LabelInference do
  def dummy_child_label_for(child_label)
    if child_label.scoped?
      "#{child_label.text}foo"
    else
      child_label.text
    end
  end

  def tree_for_parent_label(label_text)
    described_class::LABELS_TAXONOMY.find { |tree| tree.parent_label.text == label_text }
  end

  def label_for(label_text)
    described_class::Label.new(label_text)
  end

  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open',
        from_gitlab_org?: true,
        from_runbooks?: false,
        label_names: label_names,
        added_label_names: added_label_names,
        removed_label_names: removed_label_names,
        noteable_path: '/foo'
      }
    end
    let(:label_names) { added_label_names }
    let(:added_label_names) { [dummy_child_label_for(described_class::LABELS_TAXONOMY.first.child_labels.first)] }
    let(:removed_label_names) { [] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update", "merge_request.approval", "merge_request.approved", "merge_request.close", "merge_request.merge", "merge_request.note", "merge_request.update", "merge_request.open", "merge_request.reopen", "merge_request.unapproval", "merge_request.unapproved"]

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is gitlab-com/runbooks' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
        allow(event).to receive(:from_runbooks?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when there is no child label' do
      let(:added_label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when event project is not under gitlab-org nor gitlab-com/runbooks' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
        allow(event).to receive(:from_runbooks?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is no parent label' do
      described_class::LABELS_TAXONOMY.each do |label_tree|
        label_tree.child_labels.each do |child_label|
          context "with new child label" do
            let(:added_label_names) { [dummy_child_label_for(child_label)] }

            include_examples 'event is applicable'
          end
        end
      end
    end

    described_class::LABELS_TAXONOMY.each do |label_tree|
      context "with existing parent label '#{label_tree.parent_label}'" do
        label_tree.child_labels.each do |child_label|
          context "with new matching child label #{child_label.text}" do
            let(:added_label_names) { [dummy_child_label_for(child_label)] }
            let(:label_names) { [label_tree.parent_label.to_s] + added_label_names }

            # adding availability::limit triggers adding 2 labels
            # so applicable? is always true in this test given the test setup only contains one parent label
            if child_label.to_s == 'availability::limit'
              include_examples 'event is applicable'
            else
              include_examples 'event is not applicable'
            end
          end
        end
      end
    end

    described_class::LABELS_TAXONOMY.each do |label_tree|
      context "with new parent label '#{label_tree.parent_label}'" do
        label_tree.child_labels.each do |child_label|
          context "with new matching child label #{child_label.text}" do
            let(:added_label_names) { [label_tree.parent_label.to_s, dummy_child_label_for(child_label)] }

            # adding availability::limit triggers adding 2 labels
            # so applicable? is always true in this test given the test setup only contains one parent label
            if child_label.to_s == "availability::limit"
              include_examples 'event is applicable'
            else
              include_examples 'event is not applicable'
            end
          end
        end
      end
    end

    context "with existing labels 'bug::availability' and 'type::bug'" do
      context 'when availability::limit is added' do
        let(:added_label_names) { ['availability::limit'] }
        let(:label_names) { ['bug::availability', 'type::bug'] + added_label_names }

        include_examples 'event is not applicable'
      end
    end

    context "with new labels 'bug::availability' and 'type::bug'" do
      context 'when availability::limit is added' do
        let(:added_label_names) { ['availability::limit', 'bug::availability', 'type::bug'] }

        include_examples 'event is not applicable'
      end
    end

    described_class::LABELS_TAXONOMY.each do |label_tree|
      context "with existing parent label '#{described_class::LABELS_TAXONOMY[described_class::LABELS_TAXONOMY.index(label_tree) - 1].parent_label}'" do
        label_tree.child_labels.each do |child_label|
          context "with new child label for a different parent" do
            let(:added_label_names) { [dummy_child_label_for(child_label)] }
            let(:label_names) { [described_class::LABELS_TAXONOMY[described_class::LABELS_TAXONOMY.index(label_tree) - 1].parent_label.to_s] + added_label_names }

            if child_label.has_multiple_parents?
              include_examples 'event is not applicable'
            else
              include_examples 'event is applicable'
            end
          end
        end
      end
    end

    described_class::LABELS_TAXONOMY.each do |label_tree|
      context "with new parent label '#{described_class::LABELS_TAXONOMY[described_class::LABELS_TAXONOMY.index(label_tree) - 1].parent_label}'" do
        label_tree.child_labels.each do |child_label|
          context "with new child label for a different parent" do
            let(:added_label_names) { [described_class::LABELS_TAXONOMY[described_class::LABELS_TAXONOMY.index(label_tree) - 1].parent_label.to_s, dummy_child_label_for(child_label)] }

            if child_label.has_multiple_parents?
              include_examples 'event is not applicable'
            else
              include_examples 'event is applicable'
            end
          end
        end
      end
    end

    described_class::WORK_TYPE_LABEL_TREES.each do |label_tree|
      context "with worktype label #{label_tree.parent_label.text} removed" do
        let(:added_label_names) { [] }
        let(:removed_label_names) { [label_tree.parent_label.to_s] }

        context "with orphaned subtype labels #{label_tree.child_labels.map(&:to_s).join(',')}" do
          let(:label_names) { label_tree.child_labels.map(&:to_s) }

          include_examples 'event is applicable'
        end

        context 'without any orphaned subtype label' do
          let(:label_names) { [] }

          include_examples 'event is not applicable'
        end
      end
    end

    describe 'specific cases' do
      %w[type::feature type::bug].each do |parent_label|
        context "with existing parent label '#{parent_label}'" do
          context "with new child label 'security'" do
            let(:added_label_names) { ['security'] }
            let(:label_names) { [parent_label] + added_label_names }

            include_examples 'event is not applicable'
          end
        end
      end

      context "with existing parent labels 'Engineering Allocation' and 'type::feature', and added label 'Eng-Consumer::Quality'" do
        let(:label_names) { ['Engineering Allocation', 'type::feature'] + added_label_names }
        let(:added_label_names) { ['Eng-Consumer::Quality'] }

        include_examples 'event is not applicable'
      end

      context "with existing parent labels 'Engineering Allocation' and 'type::feature', and added label 'feature::addition'" do
        let(:label_names) { ['Engineering Allocation', 'type::feature'] + added_label_names }
        let(:added_label_names) { ['feature::addition'] }

        include_examples 'event is not applicable'
      end

      context "with no existing labels, and added labels 'Engineering Productivity', 'maintenance::workflow'" do
        let(:added_label_names) { ['Engineering Productivity', 'maintenance::workflow'] }

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'adding and removing labels' do |*added_and_removed_labels|
      it "adds the #{added_and_removed_labels[0]} labels, and remove the #{added_and_removed_labels[1..].flatten} labels" do
        added_labels = added_and_removed_labels.shift
        removed_labels = added_and_removed_labels.flatten
        body = ''
        body += "/label #{added_labels.map { |l| %Q(~"#{l}") }.join(' ')}" if added_labels.any?
        body += "\n/unlabel #{removed_labels.map { |l| %Q(~"#{l}") }.join(' ')}" if removed_labels.any?

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    shared_examples 'not changing labels' do
      it 'does not change labels' do
        expect_no_request do
          subject.process
        end
      end
    end

    describe 'changes to parent labels' do
      context "with no parent labels and added ['feature::addition', 'Eng-Consumer::Quality', 'ep::pipelines'] labels" do
        let(:label_names) { added_label_names }
        let(:added_label_names) { ['feature::addition', 'Eng-Consumer::Quality', 'ep::pipelines'] }

        # It sets 'type::feature', 'Engineering Allocation', 'Engineering Productivity' since they're not in the same label group
        it_behaves_like 'adding and removing labels', ['type::feature', 'Engineering Allocation', 'Engineering Productivity']
      end

      context "with no parent labels and added ['Engineering Productivity', 'maintenance::workflow'] labels" do
        let(:label_names) { added_label_names }
        let(:added_label_names) { ['Engineering Productivity', 'maintenance::workflow'] }

        it_behaves_like 'adding and removing labels', ['type::maintenance']
      end

      context "with existing parent label 'type::feature' and added ['Eng-Consumer::Quality'] label" do
        let(:label_names) { ['type::feature'] + added_label_names }
        let(:added_label_names) { ['Eng-Consumer::Quality'] }

        # It sets 'Engineering Allocation' since it's not in the same label group as 'type::feature'
        # It does not remove the 'type::feature' label since it's not in the same label group as 'Engineering Allocation'
        it_behaves_like 'adding and removing labels', ['Engineering Allocation']
      end

      context "with existing parent labels 'type::bug', 'type::maintenance' and added ['feature::addition', 'Eng-Consumer::Quality', 'ep::pipelines'] labels" do
        let(:label_names) { %w[type::bug type::maintenance] + added_label_names }
        let(:added_label_names) { ['feature::addition', 'Eng-Consumer::Quality', 'ep::pipelines'] }

        # It sets 'type::feature', 'Engineering Allocation', 'Engineering Productivity' since they're not in the same label group
        # It removes 'type::bug' and 'type::maintenance' since they're in the same label group as 'type::feature'
        it_behaves_like 'adding and removing labels', ['type::feature', 'Engineering Allocation', 'Engineering Productivity'], %w[type::maintenance type::bug]
      end

      context "with existing parent labels 'type::feature', 'type::maintenance' and added ['maintenance::pipelines'] label" do
        let(:label_names) { %w[type::feature type::maintenance] + added_label_names }
        let(:added_label_names) { ['maintenance::pipelines'] }

        # It removes 'type::feature' since they're in the same label group as 'type::maintenance'
        it_behaves_like 'adding and removing labels', [], ['type::feature']
      end

      context "with new parent label 'Engineering Productivity' and added ['feature::addition', 'Eng-Consumer::Quality', 'ep::pipelines', 'Engineering Productivity'] labels" do
        let(:label_names) { %w[type::bug type::maintenance] + added_label_names }
        let(:added_label_names) { ['feature::addition', 'Eng-Consumer::Quality', 'ep::pipelines', 'Engineering Productivity'] }

        # It sets 'type::feature', 'Engineering Allocation' since they're not in the same label group
        # It removes 'type::bug' and 'type::maintenance' since they're in the same label group as 'type::feature'
        it_behaves_like 'adding and removing labels', ['type::feature', 'Engineering Allocation'], %w[type::maintenance type::bug]
      end

      context "with no parent labels and added ['security'] label" do
        let(:label_names) { added_label_names }
        let(:added_label_names) { ['security'] }

        it_behaves_like 'not changing labels'
      end

      context "with no parent labels and added 'availability::limit'" do
        let(:added_label_names) { ['availability::limit'] }

        it_behaves_like 'adding and removing labels', ['type::bug', 'bug::availability']
      end

      context "with no parent labels and added 'availability::limit' and 'bug::availability'" do
        let(:added_label_names) { ['availability::limit', 'bug::availability'] }

        it_behaves_like 'adding and removing labels', ['type::bug']
      end

      context "with existing parent label 'type::bug'" do
        let(:label_names) { ['type::bug'] + added_label_names }

        context "with new label 'feature::addition'" do
          let(:added_label_names) { ['feature::addition'] }

          it_behaves_like 'adding and removing labels', ['type::feature'], ['type::bug']
        end

        context "with new label 'maintenance::pipelines'" do
          let(:added_label_names) { ['maintenance::pipelines'] }

          it_behaves_like 'adding and removing labels', ['type::maintenance'], ['type::bug']
        end

        context "with new label 'security'" do
          let(:added_label_names) { ['security'] }

          it_behaves_like 'not changing labels'
        end

        context "with new label 'availability::limit'" do
          let(:added_label_names) { ['availability::limit'] }

          it_behaves_like 'adding and removing labels', ['bug::availability']
        end
      end

      context "with existing parent label 'type::feature'" do
        let(:label_names) { ['type::feature'] + added_label_names }

        context "with new label 'feature::addition'" do
          let(:added_label_names) { ['feature::addition'] }

          it_behaves_like 'not changing labels'
        end

        context "with new label 'maintenance::pipelines'" do
          let(:added_label_names) { ['maintenance::pipelines'] }

          it_behaves_like 'adding and removing labels', ['type::maintenance'], ['type::feature']
        end

        context "with new label 'security'" do
          let(:added_label_names) { ['security'] }

          it_behaves_like 'not changing labels'
        end
      end

      context "with existing parent label 'type::maintenance'" do
        let(:label_names) { ['type::maintenance'] + added_label_names }

        context "with new label 'feature::addition'" do
          let(:added_label_names) { ['feature::addition'] }

          it_behaves_like 'adding and removing labels', ['type::feature'], ['type::maintenance']
        end

        context "with new label 'bug::vulnerability'" do
          let(:added_label_names) { ['bug::vulnerability'] }

          it_behaves_like 'adding and removing labels', ['type::bug'], ['type::maintenance']
        end

        context "with new label 'security'" do
          let(:added_label_names) { ['security'] }

          it_behaves_like 'not changing labels'
        end
      end

      context "with existing parent label 'type::maintenance'" do
        let(:label_names) { ['type::maintenance'] + added_label_names }

        context "with new label 'feature::addition'" do
          let(:added_label_names) { ['feature::addition'] }

          it_behaves_like 'adding and removing labels', ['type::feature'], ['type::maintenance']
        end

        context "with new label 'maintenance::pipelines'" do
          let(:added_label_names) { ['maintenance::pipelines'] }

          it_behaves_like 'not changing labels'
        end

        context "with new label 'security'" do
          let(:added_label_names) { ['security'] }

          it_behaves_like 'not changing labels'
        end
      end
    end

    describe 'changes to both parent and child labels' do
      context "with existing labels ['type::maintenance', 'maintenance::pipelines', 'type::bug', 'bug::vulnerability']" do
        let(:label_names) { ['type::maintenance', 'maintenance::pipelines', 'type::bug', 'bug::vulnerability'] + added_label_names }

        context "with new label 'feature::addition'" do
          let(:added_label_names) { ['feature::addition'] }

          it_behaves_like 'adding and removing labels', ['type::feature'], ['type::maintenance', 'type::bug', 'maintenance::pipelines', 'bug::vulnerability']
        end

        context "with new label 'maintenance::pipelines'" do
          let(:added_label_names) { ['maintenance::workflow'] }

          it_behaves_like 'adding and removing labels', [], ['type::bug', 'bug::vulnerability']
        end

        context "with new label 'security'" do
          let(:added_label_names) { ['security'] }

          it_behaves_like 'not changing labels'
        end
      end
    end

    describe 'with parent label removed' do
      context "when work type label 'type::bug' is removed" do
        let(:removed_label_names) { ['type::bug'] }
        let(:added_label_names) { [] }

        context "with subtype labels ['availability::limit', 'bug::availability'] remaining" do
          let(:label_names) { ['availability::limit', 'bug::availability'] }

          it_behaves_like 'adding and removing labels', [], ['availability::limit', 'bug::availability']
        end

        context "with subtype label 'bug::availability' added" do
          let(:added_label_names) { ['bug::availability'] }
          let(:label_names) { ['availability::limit'] }

          it_behaves_like 'adding and removing labels', ['type::bug'], []
        end

        context 'with non-work-type child label remaining' do
          let(:label_names) { ['ep::triage'] }

          it_behaves_like 'not changing labels'
        end

        context "with a different work type label 'type::feature' added" do
          let(:added_label_names) { ['type::feature'] }
          let(:label_names) { ['bug::some bug', *added_label_names] }

          it_behaves_like 'adding and removing labels', [], ['bug::some bug']
        end
      end
    end

    describe 'with child label removed' do
      let(:added_label_names) { [] }

      context 'when availablity::limit is removed and type::bug, bug::availablity, and Engineering Productivity remaining' do
        let(:label_names) { ['type::bug', 'bug::availablity', 'Engineering Productivity'] }
        let(:removed_label_names) { ['availablity::limit'] }

        it_behaves_like 'not changing labels'
      end

      context 'when maintenance::workflow is removed and type::bug and Engineering Productivity remaining' do
        let(:label_names) { ['type::maintenance', 'Engineering Productivity'] }
        let(:removed_label_names) { ['maintenance::workflow'] }

        it_behaves_like 'not changing labels'
      end
    end

    describe '#subtype_label_orphaned?' do
      let(:added_label_names) { [] }

      it 'raises error with invalid argument' do
        expect { subject.send(:subtype_label_orphaned?, 'subtype_not_found::workflow') }.to raise_error(ArgumentError)
      end

      context 'with event labeled with type::bug' do
        let(:label_names) { 'type::bug' }

        it 'returns false for bug::availability' do
          expect(subject.send(:subtype_label_orphaned?, 'bug::availability')).to be false
        end

        it 'returns true for maintenance::workflow' do
          expect(subject.send(:subtype_label_orphaned?, 'maintenance::workflow')).to be true
        end
      end

      context 'with event labeled with type::ignore' do
        let(:label_names) { 'type::ignore' }

        it 'returns true for any subtype' do
          expect(subject.send(:subtype_label_orphaned?, 'bug::')).to be true
          expect(subject.send(:subtype_label_orphaned?, 'feature::')).to be true
          expect(subject.send(:subtype_label_orphaned?, 'maintenance::')).to be true
        end
      end

      context "with event's added_label_names include type::feature" do
        let(:added_label_names) { ['type::feature'] }

        it 'returns false for feature:: subtype' do
          expect(subject.send(:subtype_label_orphaned?, 'feature::boo')).to be false
        end

        it 'returns true for bug:: and maintenance:: subtypes' do
          expect(subject.send(:subtype_label_orphaned?, 'bug::boo')).to be true
          expect(subject.send(:subtype_label_orphaned?, 'maintenance::boo')).to be true
        end
      end
    end
  end
end
