# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/thank_contribution'

RSpec.describe Triage::ThankContribution do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        event_actor_username: 'root'
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  describe '#applicable?' do
    let(:wider_community_author) { true }
    let(:from_gitlab_org) { true }

    before do
      allow(event).to receive(:wider_community_author?).and_return(wider_community_author)
      allow(event).to receive(:from_gitlab_org?).and_return(from_gitlab_org)
    end

    it_behaves_like 'event is applicable'

    context 'when event author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when event is not from gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when for project with custom conditions' do
      let(:project_id) { described_class::WWW_GITLAB_COM_PROJECT_ID }

      it 'processes different conditions' do
        expect(event).to receive(:wider_community_author?).and_return(true)
        expect(event).to receive(:from_www_gitlab_com?).and_return(true)

        expect(subject.applicable?).to be_truthy
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:expected_message) do
      add_automation_suffix('community/thank_contribution.rb') do
        <<~MARKDOWN.chomp
          Hey @root! :wave:

          Thank you for your contribution to GitLab. Please refer to the [contribution flow documentation](https://docs.gitlab.com/ee/development/contributing/#contribution-flow) for a quick overview of the process, and the [merge request (MR) guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines) for the detailed process.

          When you're ready for a first review, post `@gitlab-bot ready`. If you know a relevant reviewer(s) (for example, someone that was involved in a related issue), you can also assign them directly with `@gitlab-bot ready @user1 @user2`.

          At any time, if you need help moving the MR forward, feel free to post `@gitlab-bot help`. Read more on [how to get help](https://about.gitlab.com/community/contribute/#getting-help).

          To enable automated checks on your MR, please [configure Danger for your fork](https://docs.gitlab.com/ee/development/dangerbot.html#configuring-danger-for-forks).

          You can comment `@gitlab-bot label <label1> <label2>` to add labels to your MR. Please see the list of allowed labels in the [`label` command documentation](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#reactive-label-command).

          /label ~"Community contribution" ~"workflow::in dev"
          /assign @root
        MARKDOWN
      end
    end

    it 'posts a default message' do
      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end

    context 'when GitLab FOSS' do
      let(:project_id) { 13_083 }
      let(:expected_message) do
        add_automation_suffix('community/thank_contribution.rb') do
          <<~MARKDOWN.chomp
            Hey @root! :wave:

            Thank you for your contribution. GitLab has moved to a single codebase for GitLab CE and GitLab EE.

            Please do not create merge requests here. Instead, create them at https://gitlab.com/gitlab-org/gitlab/-/merge_requests.

            /close
            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end
      end

      it 'posts custom project comment' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end
    end

    context 'when runner' do
      let(:project_id) { 250_833 }
      let(:expected_message) do
        add_automation_suffix('community/thank_contribution.rb') do
          <<~MARKDOWN.chomp
            Hey @root! :wave:

            Thank you for your contribution to GitLab. Please refer to the [contribution flow documentation](https://docs.gitlab.com/ee/development/contributing/#contribution-flow) for a quick overview of the process, and the [merge request (MR) guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines) for the detailed process.

            When you're ready for a first review, post `@gitlab-bot ready`. If you know a relevant reviewer(s) (for example, someone that was involved in a related issue), you can also assign them directly with `@gitlab-bot ready @user1 @user2`.

            At any time, if you need help moving the MR forward, feel free to post `@gitlab-bot help`. Read more on [how to get help](https://about.gitlab.com/community/contribute/#getting-help).

            Some contributions require several iterations of review and we try to mentor contributors
            during this process. However, we understand that some reviews can be very time consuming.
            If you would prefer for us to continue the work you've submitted now or at any point in the
            future please let us know.

            If you're okay with being part of our review process (and we hope you are!), there are
            several initial checks we ask you to make:

            * The merge request description clearly explains:
              * The problem being solved.
              * The best way a reviewer can test your changes (is it possible to provide an example?).
            * If the pipeline failed, do you need help identifying what failed?
            * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
            * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/CONTRIBUTING.md#contribute-to-gitlab-runner)
            document.

            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end
      end

      it 'posts custom project comment' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end
    end

    context 'when website' do
      let(:project_id) { 7764 }
      let(:expected_message) do
        add_automation_suffix('community/thank_contribution.rb') do
          <<~MARKDOWN.chomp
            Hey @root! :wave:

            Thank you for your contribution to GitLab. Please refer to the [contribution flow documentation](https://docs.gitlab.com/ee/development/contributing/#contribution-flow) for a quick overview of the process, and the [merge request (MR) guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines) for the detailed process.

            I'll notify the Website team about your Merge Request and they will get back to you as soon
            as they can.
            If you don't hear from someone in a reasonable amount of time, please ping us again in a
            comment and mention @gitlab-com-community.

            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end
      end

      it 'posts custom project comment' do
        expect_comment_request(event: event, body: expected_message) do
          subject.process
        end
      end
    end
  end
end
