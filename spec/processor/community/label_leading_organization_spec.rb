# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/label_leading_organization'

RSpec.describe Triage::LabelLeadingOrganization do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        resource_author_id: resource_author_id
      }
    end
  end

  let(:resource_author_id) { 42 }
  let(:leading_organization) { true }
  let(:detector) { instance_double('Triage::LeadingOrganizationDetector') }

  before do
    allow(Triage::LeadingOrganizationDetector).to receive(:new).and_return(detector)
    allow(detector).to receive(:leading_organization?).with(resource_author_id).and_return(leading_organization)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when ~"Leading Organization" label is present' do
      let(:label_names) { [Labels::LEADING_ORGANIZATION_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when MR author is not from a leading organization' do
      let(:leading_organization) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = <<~MARKDOWN.chomp
        /label ~"#{Labels::LEADING_ORGANIZATION_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
