# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/type_label_nudger'
require_relative '../../triage/triage/event'

RSpec.describe Triage::TypeLabelNudger do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:gitlab_bot_event_actor) { false }
    let(:event_attrs) do
      {
        action: 'open',
        resource_open?: resource_open,
        object_kind: 'merge_request',
        from_part_of_product_project?: true,
        with_project_id?: true,
        gitlab_bot_event_actor?: gitlab_bot_event_actor
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  let(:resource_open) { true }
  let(:discussion_id) { 999 }
  let(:note_id) { 1122 }
  let(:discussion_path) { "/projects/123/merge_requests/456/discussions/#{discussion_id}" }
  let(:discussion_comment_path) { "/projects/123/merge_requests/456/discussions/#{discussion_id}/notes/#{note_id}" }
  let(:discussion_comment) { "#{comment_mark} processor message" }
  let(:resolved_discussion_comment) do
    <<~SUMMARY
      <details>
      <summary markdown="span">#{described_class::COLLAPSIBLE_COMMENT_TITLE}</summary>

      #{discussion_comment}
      </details>
    SUMMARY
  end

  let(:merge_request_discussions) do
    [id: discussion_id, notes: merge_request_notes]
  end

  include_examples 'applicable on contextual event'

  before do
    stub_api_request(
      path: '/projects/123/merge_requests/456/discussions',
      query: { per_page: 100 },
      response_body: merge_request_discussions
    )
  end

  describe '#applicable?' do
    context 'when it is not opened' do
      let(:resource_open) { false }

      include_examples 'event is not applicable'
    end

    context 'when it is a community contribution' do
      let(:label_names) { ['Community contribution', 'another_label'] }

      include_examples 'event is not applicable'
    end

    context 'when event was authored by gitlab bot' do
      let(:gitlab_bot_event_actor) { true }

      include_examples 'event is not applicable'
    end

    context 'when there is no previous discussion from the processor' do
      let(:merge_request_notes) do
        [
          { body: 'review discussion 1' },
          { body: 'review discussion 2' }
        ]
      end

      context 'with no type and subtype labels' do
        let(:label_names) { ['another_label'] }

        include_examples 'event is applicable'
      end

      context 'with type and subtype labels' do
        let(:label_names) { ['type::bug', 'bug::mobile'] }

        include_examples 'event is not applicable'
      end

      context 'with a type label, no subtype label' do
        let(:label_names) { ['type::bug'] }

        include_examples 'event is applicable'
      end

      context 'with a subtype label, no type label' do
        let(:label_names) { ['bug::mobile'] }

        include_examples 'event is applicable'
      end

      context 'with a type::ignore label' do
        let(:label_names) { ['type::ignore'] }

        include_examples 'event is applicable'
      end

      context 'with a type::ignore label, and a subtype label' do
        let(:label_names) { ['type::ignore', 'bug::mobile'] }

        include_examples 'event is applicable'
      end
    end

    context 'when there is a discussion from the processor' do
      context 'when that discussion is not resolved' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { id: note_id, body: discussion_comment, resolved: false }
          ]
        end

        context 'with no type and subtype labels' do
          let(:label_names) { ['another_label'] }

          include_examples 'event is not applicable'
        end

        context 'with type and subtype labels' do
          let(:label_names) { ['type::bug', 'bug::mobile'] }

          include_examples 'event is applicable'
        end

        context 'with a type label, no subtype label' do
          let(:label_names) { ['type::bug'] }

          include_examples 'event is not applicable'
        end

        context 'when a subtype label, no type label' do
          let(:label_names) { ['bug::mobile'] }

          include_examples 'event is not applicable'
        end

        context 'with a type::ignore label' do
          let(:label_names) { ['type::ignore'] }

          include_examples 'event is not applicable'
        end

        context 'with a type::ignore label, and a subtype label' do
          let(:label_names) { ['type::ignore', 'bug::mobile'] }

          include_examples 'event is not applicable'
        end
      end

      context 'when that discussion is resolved' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { id: note_id, body: discussion_comment, resolved: true }
          ]
        end

        context 'with no type and subtype labels' do
          let(:label_names) { ['another_label'] }

          include_examples 'event is applicable'
        end

        context 'with type and subtype labels' do
          let(:label_names) { ['type::bug', 'bug::mobile'] }

          include_examples 'event is not applicable'
        end

        context 'with a type label, no subtype label' do
          let(:label_names) { ['type::bug'] }

          include_examples 'event is applicable'
        end

        context 'with a subtype label, no type label' do
          let(:label_names) { ['bug::mobile'] }

          include_examples 'event is applicable'
        end

        context 'with a type::ignore label' do
          let(:label_names) { ['type::ignore'] }

          include_examples 'event is applicable'
        end

        context 'with a type::ignore label, and a subtype label' do
          let(:label_names) { ['type::ignore', 'bug::mobile'] }

          include_examples 'event is applicable'
        end
      end
    end

    describe 'project from which we execute the command' do
      # Make all other conditions successful, to only test the "project part of product" condition
      let(:label_names) { ['another_label'] }
      let(:merge_request_notes) do
        [
          { body: 'review discussion 1' },
          { body: 'review discussion 2' }
        ]
      end

      context 'when the project is part of the product' do
        before do
          allow(event).to receive(:from_part_of_product_project?).and_return(true)
        end

        include_examples 'event is applicable'
      end

      context 'when the project is not part of the product' do
        before do
          allow(event).to receive(:from_part_of_product_project?).and_return(false)
        end

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when no discussions have been created by the processor' do
      let(:merge_request_notes) do
        [
          { body: 'review discussion 1' },
          { body: 'review discussion 2' }
        ]
      end

      context 'with no type and subtype labels' do
        let(:label_names) { ['unrelated-label'] }

        it 'schedules a TypeLabelNudgerJob 5 minutes later' do
          expect(Triage::TypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

          subject.process
        end
      end

      context 'with type and subtype labels' do
        let(:label_names) { ['type::maintenance', 'maintenance::refactor'] }

        it 'does nothing' do
          subject.process
        end
      end

      context 'with a type label, no subtype label' do
        let(:label_names) { ['type::maintenance'] }

        it 'schedules a TypeLabelNudgerJob 5 minutes later' do
          expect(Triage::TypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

          subject.process
        end
      end

      context 'with a subtype label, no type label' do
        let(:label_names) { ['maintenance::refactor'] }

        it 'schedules a TypeLabelNudgerJob 5 minutes later' do
          expect(Triage::TypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

          subject.process
        end
      end

      context 'with a type::ignore label' do
        let(:label_names) { ['type::ignore'] }

        it 'schedules a TypeLabelNudgerJob 5 minutes later' do
          expect(Triage::TypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

          subject.process
        end
      end

      context 'with a type::ignore label, and a subtype label' do
        let(:label_names) { ['type::ignore', 'maintenance::refactor'] }

        it 'schedules a TypeLabelNudgerJob 5 minutes later' do
          expect(Triage::TypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

          subject.process
        end
      end
    end

    context 'when a previous discussion has been created by the processor' do
      context 'when that discussion is not resolved' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { id: note_id, body: discussion_comment, resolved: resolved }
          ]
        end
        let(:resolved) { false }

        context 'with no type and subtype labels' do
          let(:label_names) { ['unrelated-label'] }

          it 'does not schedule a TypeLabelNudgerJob' do
            expect(Triage::TypeLabelNudgerJob).not_to receive(:perform_in)

            subject.process
          end

          it 'does nothing' do
            subject.process
          end
        end

        context 'with type and subtype labels' do
          let(:label_names) { ['type::maintenance', 'maintenance::refactor'] }

          it 'updates and resolves the previous discussion' do
            expect_api_requests do |requests|
              requests << stub_api_request(verb: :put, path: discussion_comment_path, request_body: { 'body' => resolved_discussion_comment })
              requests << stub_api_request(verb: :put, path: discussion_path, request_body: { 'resolved' => true })

              subject.process
            end
          end
        end

        context 'with a type label, no subtype label' do
          let(:label_names) { ['type::maintenance'] }

          it 'does not schedule a TypeLabelNudgerJob' do
            expect(Triage::TypeLabelNudgerJob).not_to receive(:perform_in)

            subject.process
          end

          it 'does nothing' do
            subject.process
          end
        end

        context 'with a subtype label, no type label' do
          let(:label_names) { ['maintenance::refactor'] }

          it 'does not schedule a TypeLabelNudgerJob' do
            expect(Triage::TypeLabelNudgerJob).not_to receive(:perform_in)

            subject.process
          end

          it 'does nothing' do
            subject.process
          end
        end

        context 'with a type::ignore label' do
          let(:label_names) { ['type::ignore'] }

          it 'does not schedule a TypeLabelNudgerJob' do
            expect(Triage::TypeLabelNudgerJob).not_to receive(:perform_in)

            subject.process
          end

          it 'does nothing' do
            subject.process
          end
        end

        context 'with a type::ignore label, and a subtype label' do
          let(:label_names) { ['type::ignore', 'maintenance::refactor'] }

          it 'does not schedule a TypeLabelNudgerJob' do
            expect(Triage::TypeLabelNudgerJob).not_to receive(:perform_in)

            subject.process
          end

          it 'does nothing' do
            subject.process
          end
        end
      end

      context 'when that discussion is resolved' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { id: note_id, body: resolved_discussion_comment, resolved: resolved }
          ]
        end
        let(:resolved) { true }

        context 'with no type and subtype labels' do
          let(:label_names) { ['unrelated-label'] }

          it 'updates and unresolves the previous discussion' do
            expect_api_requests do |requests|
              requests << stub_api_request(verb: :put, path: discussion_comment_path, request_body: { 'body' => discussion_comment })
              requests << stub_api_request(verb: :put, path: discussion_path, request_body: { 'resolved' => false })

              subject.process
            end
          end
        end

        context 'with type and subtype labels' do
          let(:label_names) { ['type::maintenance', 'maintenance::refactor'] }

          it 'does nothing' do
            subject.process
          end
        end

        context 'with a type label, no subtype label' do
          let(:label_names) { ['type::maintenance'] }

          it 'does nothing' do
            subject.process
          end
        end

        context 'with a subtype label, no type label' do
          let(:label_names) { ['maintenance::refactor'] }

          it 'updates and unresolves the previous discussion' do
            expect_api_requests do |requests|
              requests << stub_api_request(verb: :put, path: discussion_comment_path, request_body: { 'body' => discussion_comment })
              requests << stub_api_request(verb: :put, path: discussion_path, request_body: { 'resolved' => false })

              subject.process
            end
          end
        end

        context 'with a type::ignore label' do
          let(:label_names) { ['type::ignore'] }

          it 'unresolves the previous discussion' do
            expect_api_requests do |requests|
              requests << stub_api_request(verb: :put, path: discussion_comment_path, request_body: { 'body' => discussion_comment })
              requests << stub_api_request(verb: :put, path: discussion_path, request_body: { 'resolved' => false })

              subject.process
            end
          end
        end

        context 'with a type::ignore label, and a subtype label' do
          let(:label_names) { ['type::ignore', 'maintenance::refactor'] }

          it 'updates and unresolves the previous discussion' do
            expect_api_requests do |requests|
              requests << stub_api_request(verb: :put, path: discussion_comment_path, request_body: { 'body' => discussion_comment })
              requests << stub_api_request(verb: :put, path: discussion_path, request_body: { 'resolved' => false })

              subject.process
            end
          end
        end
      end
    end
  end
end
