# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'
require_relative '../../lib/team_member_select_helper'

RSpec.describe TeamMemberSelectHelper do
  let(:resource_klass) do
    Class.new do
      include TeamMemberSelectHelper
    end
  end

  let(:team_from_www) do
    {
      'first_coach' => {
        'departments' => ['Quality Department', 'Merge Request coach'],
        'role' => '<a href="/job-families/engineering/development/backend/senior/">Senior Backend Engineer, Release</a>'
      },
      'second_coach' => {
        'departments' => ['Quality Department', 'Merge Request coach'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Fulfillment:Fulfillment Platform</a>'
      },
      'unavailable_coach' => {
        'departments' => ['Quality Department', 'Merge Request coach'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Fulfillment:Fulfillment Platform</a>'
      },
      'not_coach' => {
        'departments' => ['Quality Department'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Fulfillment:Fulfillment Platform</a>'
      },
      'vp_quality' => {
        'departments' => ['Quality Department'],
        'role' => 'Vice President of Quality'
      },
      'ep_em' => {
        'departments' => ['Quality Department'],
        'role' => 'Engineering Manager, Engineering Productivity'
      },
      'quality_ep_em' => {
        'departments' => ['Quality Department'],
        'role' => 'Quality Engineering Manager, Engineering Productivity'
      },
      'create_editor_be' => {
        'departments' => ['Create:Editor BE Team'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Create:Editor</a>'
      },
      'create_editor_fe' => {
        'departments' => ['Create:Editor FE Team'],
        'role' => '<a href="/job-families/engineering/development/frontend">Frontend Engineer, Create:Editor</a>'
      },
      'threat_insights_be' => {
        'departments' => ['Govern:Threat Insights BE Team'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Govern:Threat Insights</a>'
      },
      'threat_insights_fe' => {
        'departments' => ['Govern:Threat Insights FE Team'],
        'role' => '<a href="/job-families/engineering/development/frontend">Frontend Engineer, Govern:Threat Insights</a>'
      },
      'security_policies_be' => {
        'departments' => ['Govern:Security Policies BE Team'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Govern:Security Policies</a>'
      },
      'security_policies_fe' => {
        'departments' => ['Govern:Security Policies BE Team'],
        'role' => '<a href="/job-families/engineering/development/frontend">Frontend Engineer, Govern:Security Policies</a>'
      },
      'dast_be' => {
        'departments' => ['Secure:Dynamic Analysis BE Team'],
        'role' => '<a href="/job-families/engineering/development/backend">Backend Engineer, Secure:Dynamic Analysis</a>'
      }
    }
  end

  let(:roulette) do
    [
      { 'username' => 'first_coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'second_coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'unavailable_coach', 'merge_request_coach' => true, 'available' => false },
      { 'username' => 'not_coach', 'merge_request_coach' => false, 'available' => true }
    ]
  end

  before do
    allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  subject { resource_klass.new }

  describe 'EDITOR_GROUP_REGEXP' do
    it do
      expect(described_class::EDITOR_GROUP_REGEXP).to eq(/editor/)
    end
  end

  describe 'THREAT_INSIGHTS_GROUP_REGEXP' do
    it do
      expect(described_class::THREAT_INSIGHTS_GROUP_REGEXP).to eq(/threat insights/)
    end
  end

  describe 'SECURITY_POLICIES_GROUP_REGEXP' do
    it do
      expect(described_class::SECURITY_POLICIES_GROUP_REGEXP).to eq(/security policies/)
    end
  end

  describe 'DYNAMIC_ANALYSIS_GROUP_REGEXP' do
    it do
      expect(described_class::DYNAMIC_ANALYSIS_GROUP_REGEXP).to eq(/dynamic analysis/)
    end
  end

  describe '#merge_request_coaches' do
    context 'with no arguments' do
      subject(:coaches) { resource_klass.new.merge_request_coaches }

      it 'retrieves merge request coaches from www-gitlab-com' do
        expect(coaches).to match_array(%w[@first_coach @second_coach])
      end
    end

    context 'when a group: is given' do
      subject(:coaches) { resource_klass.new.merge_request_coaches(group: /Fulfillment Platform/) }

      it 'retrieves merge request coaches from www-gitlab-com' do
        expect(coaches).to match_array(%w[@second_coach])
      end
    end
  end

  describe '#select_random_merge_request_coach' do
    context 'with no arguments' do
      subject(:coach) { resource_klass.new.select_random_merge_request_coach }

      it 'returns random mr coach' do
        expect(coach).to eq('@first_coach').or(eq('@second_coach'))
      end
    end

    context 'when a group: is given' do
      subject(:coach) { resource_klass.new.select_random_merge_request_coach(group: /release/) }

      it 'returns random mr coach from this group' do
        expect(coach).to eq('@first_coach')
      end
    end
  end

  describe '#untriaged_issues_triagers' do
    subject(:triagers) { resource_klass.new.untriaged_issues_triagers }

    it 'retrieves triage team from www-gitlab-com' do
      expect(triagers).to match_array(%w[@ep_em @quality_ep_em])
    end
  end

  describe '#create_editor_be' do
    it 'retrieves team members matching criteria' do
      expect(subject.create_editor_be).to eq('@create_editor_be')
    end
  end

  describe '#create_editor_fe' do
    it 'retrieves team members matching criteria' do
      expect(subject.create_editor_fe).to eq('@create_editor_fe')
    end
  end

  describe '#security_policies_be' do
    it 'retrieves team members matching criteria' do
      expect(subject.security_policies_be).to eq('@security_policies_be')
    end
  end

  describe '#security_policies_fe' do
    it 'retrieves team members matching criteria' do
      expect(subject.security_policies_fe).to eq('@security_policies_fe')
    end
  end

  describe '#threat_insights_be' do
    it 'retrieves team members matching criteria' do
      expect(subject.threat_insights_be).to eq('@threat_insights_be')
    end
  end

  describe '#threat_insights_fe' do
    it 'retrieves team members matching criteria' do
      expect(subject.threat_insights_fe).to eq('@threat_insights_fe')
    end
  end

  describe '#dast_be' do
    it 'retrieves team members matching criteria' do
      expect(subject.dast_be).to eq('@dast_be')
    end
  end

  describe '#team_member_exist?' do
    it 'returns true if the member exists' do
      expect(subject.team_member_exist?('first_coach')).to be(true)
    end

    it 'returns false if the member does not exist' do
      expect(subject.team_member_exist?('unknown')).to be(false)
    end

    it 'returns false if the username is nil' do
      expect(subject.team_member_exist?(nil)).to be(false)
    end

    it 'returns false if the member exists but is in a core team member' do
      team_from_www['core_team_member'] = { 'role' => 'Core Team member' }

      expect(subject.team_member_exist?('core_team_member')).to be(false)
    end

    # Regression test: https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1103
    it 'does not break if a member has no username' do
      team_from_www[nil] = { 'role' => 'Board member' }

      expect(subject.team_member_exist?('a_member')).to be(false)
    end
  end
end
