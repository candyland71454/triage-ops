# frozen_string_literal: true

require 'spec_helper'
require 'active_support'
require_relative '../../lib/labels_helper'

RSpec.describe LabelsHelper do
  let(:resource_klass) do
    Class.new do
      include LabelsHelper

      def label_names
      end

      def labels_with_details
      end
    end
  end

  let(:label_klass) do
    Struct.new(:name, :added_at)
  end

  let(:labels) { [] }

  subject { resource_klass.new }

  before do
    allow(subject).to receive(:labels_with_details).and_return(labels)
    allow(subject).to receive(:label_names).and_return(labels.map(&:name))
  end

  describe '#label_added_at' do
    context 'when resource has no labels' do
      it 'returns nil' do
        expect(subject.label_added_at('foo')).to be_nil
      end
    end

    context 'when resource has labels but not the given one' do
      let(:labels) { [label_klass.new('bar', Time.now.utc)] }

      it 'returns nil' do
        expect(subject.label_added_at('foo')).to be_nil
      end
    end

    context 'when resource has the given label set' do
      let(:added_at) { Time.now }
      let(:labels) { [label_klass.new('foo', added_at)] }

      it 'returns a time' do
        expect(subject.label_added_at('foo')).to eq(added_at)
      end
    end
  end

  describe '#label_added_before?' do
    context 'when resource has no labels' do
      it 'returns false' do
        expect(subject.label_added_before?('foo', Time.now.utc)).to be(false)
      end
    end

    context 'when resource has labels but not the given one' do
      let(:labels) { [label_klass.new('bar', Time.now.utc)] }

      it 'returns false' do
        expect(subject.label_added_before?('foo', Time.now)).to be(false)
      end
    end

    context 'when resource has the given label set' do
      let(:labels) { [label_klass.new('foo', added_at)] }

      context 'when label was not set before the given time' do
        let(:added_at) { Time.now }

        it 'returns false' do
          expect(subject.label_added_before?('foo', Time.now.utc - 2.weeks)).to be(false)
        end
      end

      context 'when label was set before the given time' do
        let(:added_at) { 3.weeks.ago }

        it 'returns true' do
          expect(subject.label_added_before?('foo', Time.now.utc - 2.weeks)).to be(true)
        end
      end
    end
  end

  describe '#feature_label?' do
    context 'when resource has no labels' do
      it 'returns false' do
        expect(subject.feature_label?).to be(false)
      end
    end

    context 'when resource is a feature' do
      let(:added_at) { Time.now }
      let(:labels) { [label_klass.new('type::feature', added_at)] }

      it 'returns true' do
        expect(subject.feature_label?).to be(true)
      end
    end
  end

  describe '#p1_bug_label?' do
    context 'when resource is not a bug' do
      it 'returns false' do
        expect(subject.p1_bug_label?).to be(false)
      end
    end

    context 'when resource is a bug' do
      let(:added_at) { Time.now }

      context 'when resource is p1' do
        let(:labels) { [label_klass.new('type::bug', added_at), label_klass.new('priority::1', added_at)] }

        it 'returns true' do
          expect(subject.p1_bug_label?).to be(true)
        end
      end

      context 'when resource is not p1' do
        let(:labels) { [label_klass.new('type::bug', added_at), label_klass.new('priority::2', added_at)] }

        it 'returns false' do
          expect(subject.p1_bug_label?).to be(false)
        end
      end
    end
  end
end
