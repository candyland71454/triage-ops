# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/flaky_report'

RSpec.describe FlakyReport do
  let(:project_id) { '123' }
  let(:iid) { 456 }
  let(:latest_pipeline_id) { 42 }
  let(:latest_pipeline) do
    {
      id: latest_pipeline_id,
      ref: "refs/merge-requests/#{iid}/merge",
      project_id: project_id,
      finished_at: Time.now - 3600
    }
  end
  let(:job_id) { 64 }
  let(:pipeline_job) do
    {
      name: described_class::FLAKY_REPORT_JOB_NAME,
      id: job_id,
      status: 'success'
    }
  end
  let(:flaky_report_suite) do
    {
      'SHA-HERE': {
        last_flaky_at: "2022-09-11 18:26:07 +0000",
        last_attempts_count: 2,
        flaky_reports: 64,
        example_id: "./spec/features/projects/members/manage_members_spec.rb[1:6:3]",
        file: "./spec/support/shared_examples/features/inviting_members_shared_examples.rb"
      }
    }
  end

  subject { described_class.new(project_path: project_id, token: 'TOKEN') }

  describe '#latest_flaky_report' do
    it 'prints report' do
      stub_request(:get, "https://gitlab.com/api/v4/projects/#{project_id}/pipelines?per_page=1&status=success&username=gitlab-bot").to_return(body: [latest_pipeline].to_json)
      stub_request(:get, "https://gitlab.com/api/v4/projects/#{project_id}/pipelines/#{latest_pipeline_id}/jobs?per_page=100").to_return(body: [pipeline_job].to_json)
      stub_request(:get, "https://gitlab.com/api/v4/projects/#{project_id}/jobs/#{job_id}/artifacts/rspec_flaky/report-suite.json").to_return(body: flaky_report_suite.to_json)

      subject.latest_flaky_report
    end
  end
end
