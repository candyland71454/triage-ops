# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/changed_file_list'

RSpec.describe Triage::ChangedFileList do
  let(:project_id) { 42 }
  let(:merge_request_iid) { 12 }
  let(:model_change) do
    {
      'old_path' => 'app/models/user.rb',
      'new_path' => 'app/models/user.rb'
    }
  end
  let(:doc_change) do
    {
      "old_path" => "doc/user.md",
      "new_path" => "doc/user.md"
    }
  end
  let(:merge_request_with_no_doc_change) do
    {
      'changes' => [model_change]
    }
  end
  let(:merge_request_with_a_doc_change) do
    {
      'changes' => [model_change, doc_change]
    }
  end
  let(:merge_request_with_only_doc_change) do
    {
      'changes' => [doc_change]
    }
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  describe '#any_change?' do
    context 'when a regex is given' do
      subject(:any_doc_change) { described_class.new(project_id, merge_request_iid).any_change?(%r{\Adoc/}) }

      context 'when merge request has no docs change' do
        let(:merge_request_changes) { merge_request_with_no_doc_change }

        it 'returns false' do
          expect(any_doc_change).to be_falsey
        end
      end

      context 'when merge request includes a docs change' do
        let(:merge_request_changes) { merge_request_with_a_doc_change }

        it 'returns true' do
          expect(any_doc_change).to be_truthy
        end
      end
    end

    context 'when an array is given' do
      subject(:any_doc_change) { described_class.new(project_id, merge_request_iid).any_change?(%w[doc/ lib/]) }

      context 'when merge request has no docs change' do
        let(:merge_request_changes) { merge_request_with_no_doc_change }

        it 'returns false' do
          expect(any_doc_change).to be_falsey
        end
      end

      context 'when merge request includes a docs change' do
        let(:merge_request_changes) { merge_request_with_a_doc_change }

        it 'returns true' do
          expect(any_doc_change).to be_truthy
        end
      end
    end

    context 'when a string is given' do
      subject(:any_doc_change) { described_class.new(project_id, merge_request_iid).any_change?('doc') }

      context 'when merge request has no docs change' do
        let(:merge_request_changes) { merge_request_with_no_doc_change }

        it 'returns false' do
          expect(any_doc_change).to be_falsey
        end
      end

      context 'when merge request includes a docs change' do
        let(:merge_request_changes) { merge_request_with_a_doc_change }

        it 'returns true' do
          expect(any_doc_change).to be_truthy
        end
      end
    end

    context 'when something else is given' do
      let(:merge_request_changes) { merge_request_with_no_doc_change }

      subject(:any_doc_change) { described_class.new(project_id, merge_request_iid).any_change?(nil) }

      it 'raises an ArgumentError error' do
        expect { any_doc_change }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#only_change?' do
    subject(:only_doc_change) { described_class.new(project_id, merge_request_iid).only_change?(%r{\Adoc/}) }

    context 'when merge request has no docs change' do
      let(:merge_request_changes) { merge_request_with_no_doc_change }

      it 'returns false' do
        expect(only_doc_change).to be_falsey
      end
    end

    context 'when merge request includes a docs change' do
      let(:merge_request_changes) { merge_request_with_a_doc_change }

      it 'returns false' do
        expect(only_doc_change).to be_falsey
      end
    end

    context 'when merge request includes only docs change' do
      let(:merge_request_changes) { merge_request_with_only_doc_change }

      it 'returns true' do
        expect(only_doc_change).to be_truthy
      end
    end
  end

  describe '#each_change' do
    subject { described_class.new(project_id, merge_request_iid) }

    context 'when merge request has no docs change' do
      let(:merge_request_changes) { merge_request_with_no_doc_change }

      it 'yields nothing when asking docs change' do
        yielded = []

        subject.each_change(%r{\Adoc/}) do
          yielded << change.to_h
        end

        expect(yielded).to eq([])
      end
    end

    context 'when merge request includes a docs change' do
      let(:merge_request_changes) { merge_request_with_a_doc_change }

      it 'yields changes when asking docs change' do
        yielded = []

        subject.each_change(%r{\Adoc/}) do |change|
          yielded << change.to_h
        end

        expect(yielded).to eq([doc_change])
      end
    end
  end
end
