# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/concerns/periscope_csv'

RSpec.describe Triage::PeriscopeCsv, :clean_cache do
  let(:concrete_class) do
    Class.new do
      include Triage::PeriscopeCsv

      def self.name
        'ConcretePeriscopeCsvClass'
      end
    end
  end
  let(:concrete_object) { concrete_class.new }
  let(:csv_url) { 'https://app.periscopedata.com/api/gitlab/bar' }
  let(:csv_body) do
    <<~CSV
      ATTR_1,ATTR_2
      1,a
      2,b
    CSV
  end
  let(:csv_response) { double(parsed_response: csv_body) }

  before do
    allow(HTTParty).to receive(:get).and_return(csv_response)
  end

  shared_context 'with a valid CSV URL' do
    before do
      env_var_key = "TEST_#{described_class}_CSV_URL_VAR"
      concrete_class.const_set(:CSV_URL_VAR, env_var_key)
      allow(ENV).to receive(:fetch).with(env_var_key).and_return(csv_url)
    end
  end

  describe '#csv_map' do
    context 'when CSV_URL_VAR is not defined' do
      it 'raises an exception when trying to get csv_url' do
        expect { concrete_object.csv_map }.to raise_error(NameError)
      end
    end

    context 'when ENV[CSV_URL_VAR] is set' do
      include_context 'with a valid CSV URL'

      context 'when ENV[CSV_URL_VAR] is not a valid URL' do
        let(:csv_url) { 'foo bar' }

        it 'raises an exception when trying to get csv_url' do
          expect { concrete_object.csv_map }.to raise_error(described_class::CsvUrlMissingError)
        end
      end

      context 'when ENV[CSV_URL_VAR] has the wrong host' do
        let(:csv_url) { 'foo.org/api/gitlab/bar' }

        it 'raises an exception when trying to get csv_url' do
          expect { concrete_object.csv_map }.to raise_error(described_class::CsvUrlInvalidError)
        end
      end

      context 'when ENV[CSV_URL_VAR] is valid' do
        it 'parses the CSV data' do
          expect(concrete_object.csv_map).to eq([
            { 'ATTR_1' => '1', 'ATTR_2' => 'a' },
            { 'ATTR_1' => '2', 'ATTR_2' => 'b' }
          ])
        end
      end

      it 'caches the CSV URL response', :clean_cache do
        concrete_object.csv_map

        expect(HTTParty).not_to receive(:get)

        concrete_object.csv_map
      end

      it 'expires the cache and retrieves', :clean_cache do
        concrete_object.csv_map

        # expire cache
        Timecop.travel(Time.now + described_class::CSV_CACHE_EXPIRY + 1) do
          expect(HTTParty).to receive(:get)

          concrete_object.csv_map
        end
      end
    end
  end
end
