# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/type_label_nudger_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::TypeLabelNudgerJob do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:project_id) { 999 }

    let(:event_attrs) do
      {
        action: 'open',
        from_gitlab_org?: true,
        iid: mr_iid,
        object_kind: 'merge_request',
        project_id: project_id,
        event_actor_username: 'bob'
      }
    end

    let(:mr_iid) { 1234 }
    let(:username) { 'requestor' }
  end

  # We don't reuse the one from spec/support/merge_request_notes_context.rb,
  # As calling `subject.unique_comment` will memoize a UniqueComment instance
  # with an event not defined (see the unique_comment method in TypeLabelNudgerJob)
  let(:comment_mark) do
    described_class.new.__send__(:unique_comment).__send__(:hidden_comment)
  end

  subject { described_class.new }

  describe '#perform' do
    let(:labels) { [] }
    let(:merge_request) do
      {
        project_id: project_id,
        iid: mr_iid,
        labels: labels,
        state: state
      }
    end
    let(:merge_request_discussions) do
      [id: 123, notes: merge_request_notes]
    end
    let(:merge_request_notes) { {} }
    let(:state) { 'opened' }

    before do
      stub_api_request(
        path: "/projects/#{project_id}/merge_requests/#{mr_iid}/discussions",
        query: { per_page: 100 },
        response_body: merge_request_discussions
      )
    end

    shared_examples 'not posting a reminder' do
      it 'does not post a reminder' do
        expect_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request) do
          subject.perform(event)
        end
      end
    end

    context 'when the merge request is merged' do
      let(:state) { 'merged' }

      it_behaves_like 'not posting a reminder'
    end

    context 'when the merge request is closed' do
      let(:state) { 'closed' }

      it_behaves_like 'not posting a reminder'
    end

    context 'when ~"Community contribution" label is present' do
      let(:labels) { ['Community contribution'] }

      it_behaves_like 'not posting a reminder'
    end

    context 'when a type:ignore label is present' do
      let(:labels) { ['type::ignore'] }

      context 'when no discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' },
          ]
        end

        it 'creates a discussion' do
          body = <<~MARKDOWN.chomp
            #{comment_mark}
            :wave: @#{event.resource_author.username} - please see the following guidance and update this merge request.
            | | 2 Errors, 1 Warning |
            | -------- | -------- |
            | :x: | #{described_class::TYPE_LABEL_MISSING_ERROR_MESSAGE} |
            | :x: | #{described_class::TYPE_IGNORE_LABEL_ERROR_MESSAGE} |
            | :warning: | #{described_class::SUBTYPE_LABEL_MISSING_WARNING_MESSAGE} |

            #{described_class::SUBTYPE_LABEL_FOOTER_MESSAGE}
          MARKDOWN

          expect_api_requests do |requests|
            requests << stub_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request)
            requests << stub_discussion_request(event: event, body: body)

            subject.perform(event)
          end
        end
      end

      context 'when a discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: true }
          ]
        end

        it_behaves_like 'not posting a reminder'
      end
    end

    context 'when a type label is present, and no subtype label is present' do
      let(:labels) { ['type::bug'] }

      context 'when no discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' },
          ]
        end

        it 'creates a discussion' do
          body = <<~MARKDOWN.chomp
            #{comment_mark}
            :wave: @#{event.resource_author.username} - please see the following guidance and update this merge request.
            | | 1 Warning |
            | -------- | -------- |
            | :warning: | Please add a [subtype label](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) to this merge request. |

            #{described_class::SUBTYPE_LABEL_FOOTER_MESSAGE}
          MARKDOWN

          expect_api_requests do |requests|
            requests << stub_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request)
            requests << stub_discussion_request(event: event, body: body)

            subject.perform(event)
          end
        end
      end

      context 'when a discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: true }
          ]
        end

        it_behaves_like 'not posting a reminder'
      end
    end

    context 'when a subtype label is present, and no type label is present' do
      let(:labels) { ['bug::mobile'] }

      context 'when no discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' },
          ]
        end

        it 'creates a discussion' do
          body = <<~MARKDOWN.chomp
            #{comment_mark}
            :wave: @#{event.resource_author.username} - please see the following guidance and update this merge request.
            | | 1 Error |
            | -------- | -------- |
            | :x: | #{described_class::TYPE_LABEL_MISSING_ERROR_MESSAGE} |
          MARKDOWN

          expect_api_requests do |requests|
            requests << stub_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request)
            requests << stub_discussion_request(event: event, body: body)

            subject.perform(event)
          end
        end
      end

      context 'when a discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: true }
          ]
        end

        it_behaves_like 'not posting a reminder'
      end
    end

    context 'when type and subtype labels are presnet' do
      let(:labels) { ['type::bug', 'bug::mobile'] }

      it_behaves_like 'not posting a reminder'
    end

    context 'when no type and subtype labels are present' do
      let(:labels) { [] }

      context 'when no discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: 'review discussion 2' },
          ]
        end

        it 'creates a discussion' do
          body = <<~MARKDOWN.chomp
            #{comment_mark}
            :wave: @#{event.resource_author.username} - please see the following guidance and update this merge request.
            | | 1 Error, 1 Warning |
            | -------- | -------- |
            | :x: | #{described_class::TYPE_LABEL_MISSING_ERROR_MESSAGE} |
            | :warning: | #{described_class::SUBTYPE_LABEL_MISSING_WARNING_MESSAGE} |

            #{described_class::SUBTYPE_LABEL_FOOTER_MESSAGE}
          MARKDOWN

          expect_api_requests do |requests|
            requests << stub_api_request(path: "/projects/#{project_id}/merge_requests/#{mr_iid}", response_body: merge_request)
            requests << stub_discussion_request(event: event, body: body)

            subject.perform(event)
          end
        end
      end

      context 'when a discussion from the processor is present' do
        let(:merge_request_notes) do
          [
            { body: 'review discussion 1' },
            { body: "#{comment_mark} processor message", resolved: true }
          ]
        end

        it_behaves_like 'not posting a reminder'
      end
    end
  end
end
