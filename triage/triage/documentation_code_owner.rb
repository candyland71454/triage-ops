# frozen_string_literal: true

require_relative '../triage'

module Triage
  class DocumentationCodeOwner
    DOC_SECTION_PATTERN = %r{\ADocumentation|\ADocs}
    RULE_TYPE_CODE_OWNER = 'code_owner'

    def initialize(project_id, merge_request_iid)
      @project_id = project_id
      @merge_request_iid = merge_request_iid
      @docs_approvers_by_type = nil
    end

    def approvers
      @approvers ||= doc_approval_rules.each_with_object(Set.new) do |approval_rule, approvers|
        approvers.merge(approval_rule.eligible_approvers.map(&:username))
      end.to_a
    end

    private

    attr_reader :project_id, :merge_request_iid

    def approval_rules
      Triage.api_client.get("/projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules").lazy
    end

    def doc_approval_rules
      @doc_approval_rules ||= approval_rules.select do |rule|
        rule.rule_type == RULE_TYPE_CODE_OWNER && rule.section&.match(DOC_SECTION_PATTERN)
      end
    end
  end
end
