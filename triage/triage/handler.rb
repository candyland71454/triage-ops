# frozen_string_literal: true

require_relative 'listener'

require_relative '../processor/apply_type_label_from_related_issue'
require_relative '../processor/assign_dev_for_verification'
require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/breaking_change_comment'
require_relative '../processor/copy_security_issue_labels'
require_relative '../processor/customer_label'
require_relative '../processor/default_label_upon_closing'
require_relative '../processor/deprecated_label'
require_relative '../processor/engineering_allocation_labels_reminder'
require_relative '../processor/label_inference'
require_relative '../processor/label_jihu_contribution'
require_relative '../processor/legal_disclaimer_on_direction_resources'
require_relative '../processor/merge_request_ci_title_label'
require_relative '../processor/new_pipeline_on_approval'
require_relative '../processor/notify_changes'
require_relative '../processor/vulnerability_issue_slo_reminder'
require_relative '../processor/pajamas_missing_workflow_label_or_weight'
require_relative '../processor/prod_ops_flow_notifier'
require_relative '../processor/remind_merged_mr_deviating_from_guidelines'
require_relative '../processor/remind_author_not_compliant_to_merge_mr'
require_relative '../processor/request_legal_review'
require_relative '../processor/scheduled_issue_type_label_nudger'
require_relative '../processor/require_type_on_refinement'
require_relative '../processor/seeking_community_contributions_label'
require_relative '../processor/team_label_inference'
require_relative '../processor/ux_mrs'

require_relative '../processor/appsec/approved_by_appsec'
require_relative '../processor/appsec/appsec_approval_label_added'
require_relative '../processor/appsec/ping_appsec_on_approval'
require_relative '../processor/appsec/revoke_appsec_approval'

require_relative '../processor/community/automated_review_request_doc'
require_relative '../processor/community/automated_review_request_generic'
require_relative '../processor/community/automated_review_request_ux'
require_relative '../processor/community/code_review_experience_feedback'
require_relative '../processor/community/command_mr_feedback'
require_relative '../processor/community/command_mr_help'
require_relative '../processor/community/command_mr_label'
require_relative '../processor/community/command_mr_request_review'
require_relative '../processor/community/hackathon_label'
require_relative '../processor/community/label_leading_organization'
require_relative '../processor/community/remove_idle_labels_on_activity'
require_relative '../processor/community/reset_review_state'
require_relative '../processor/community/thank_contribution'
require_relative '../processor/type_label_nudger'

module Triage
  class Handler
    DEFAULT_PROCESSORS = [
      ApplyTypeLabelFromRelatedIssue,
      AssignDevForVerification,
      AvailabilityPriority,
      BackstageLabel,
      BreakingChangeComment,
      CopySecurityIssueLabels,
      CustomerLabel,
      DefaultLabelUponClosing,
      DeprecatedLabel,
      EngineeringAllocationLabelsReminder,
      LabelInference,
      LabelJiHuContribution,
      LegalDisclaimerOnDirectionResources,
      MergeRequestCiTitleLabel,
      NewPipelineOnApproval,
      NotifyChanges,
      VulnerabilityIssueSloReminder,
      PajamasMissingWorkflowLabelOrWeight,
      ProdOpsFlowNotifier,
      RemindMergedMrDeviatingFromGuideline,
      RemindAuthorNotCompliantToMergeMr,
      ScheduledIssueTypeLabelNudger,
      RequireTypeOnRefinement,
      SeekingCommunityContributionsLabel,
      TeamLabelInference,
      TypeLabelNudger,
      UxMrs,

      # AppSec processors
      ApprovedByAppSec,
      AppSecApprovalLabelAdded,
      PingAppSecOnApproval,
      RevokeAppSecApproval,

      # Community processors
      AutomatedReviewRequestDoc,
      AutomatedReviewRequestGeneric,
      AutomatedReviewRequestUx,
      CodeReviewExperienceFeedback,
      CommandMrFeedback,
      CommandMrHelp,
      CommandMrLabel,
      CommandMrRequestReview,
      HackathonLabel,
      LabelLeadingOrganization,
      RemoveIdleLabelOnActivity,
      ResetReviewState,
      ThankContribution
    ].freeze

    Result = Struct.new(:message, :error, :duration)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |processor|
        start_time = Triage.current_monotonic_time
        results[processor.name].message = processor.triage(event)
      rescue => e
        results[processor.name].error = e
      ensure
        results[processor.name].duration = (Triage.current_monotonic_time - start_time).round(5)
      end

      results.select { |processor, result| result.message || result.error }
    end

    private

    attr_reader :event, :processors

    def listeners
      @listeners ||= processors.each_with_object(Hash.new { |h, k| h[k] = [] }) do |processor, result|
        processor.listeners.each do |listener|
          result[listener.event] << processor
        end
      end
    end
  end
end
