# frozen_string_literal: true

require_relative '../triage'

module Triage
  class LabelEventFinder
    attr_reader :project_id, :resource_iid, :resource_type, :label_name, :action

    def initialize(label_event_attr)
      @project_id, @resource_iid, @resource_type, @label_name, @action =
        label_event_attr.values_at(:project_id, :resource_iid, :resource_type, :label_name, :action)
    end

    def label_added_date
      label_event&.created_at
    end

    private

    def label_event
      @label_event ||=
        label_events_from_api.select { |event| event.action == action && event.label }
          .reverse
          .find { |event| event.label.name == label_name }
    end

    def label_events_from_api
      path = resource_type == 'issue' ? 'issue_label_events' : 'merge_request_label_events'
      # rubocop:disable GitlabSecurity/PublicSend
      @label_events_from_api ||=
        Triage.api_client.public_send(path, project_id, resource_iid).auto_paginate
      # rubocop:enable GitlabSecurity/PublicSend
    end
  end
end
