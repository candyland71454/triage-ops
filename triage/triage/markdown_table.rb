# frozen_string_literal: true

require 'active_support/core_ext/string' # to use .pluralize

module Triage
  class MarkdownTable
    attr_reader :result

    def initialize(result)
      @result = result
    end

    def markdown
      "#{table_header}#{table_rows.join("\n")}"
    end

    private

    def emoji(severity)
      case severity
      when 'errors'
        ':x:'
      else
        ':warning:'
      end
    end

    def title(severity, count)
      "#{count} #{severity.pluralize(count)}"
    end

    def table_header
      table_title = []

      table_title << title('Error', result.errors.count) if result.errors.any?
      table_title << ', ' if result.errors.any? && result.warnings.any?
      table_title << title('Warning', result.warnings.count) if result.warnings.any?

      <<~MARKDOWN
      | | #{table_title.join} |
      | -------- | -------- |
      MARKDOWN
    end

    def table_row(severity, message)
      "| #{emoji(severity)} | #{message} |"
    end

    def table_rows
      %i[errors warnings].flat_map do |severity|
        messages = result.public_send(severity) # rubocop:disable GitlabSecurity/PublicSend
        messages.map do |message|
          table_row(severity.to_s, message)
        end
      end
    end
  end
end
