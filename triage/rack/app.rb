# frozen_string_literal: true

require 'rack'
require 'rack/requestid'

require_relative 'attach_logger'
require_relative 'attach_request_id'
require_relative 'authenticator'
require_relative 'dashboard'
require_relative 'error_handler'
require_relative 'processor'
require_relative 'request_logger'
require_relative 'webhook_event'
require_relative '../triage/logging'

module Triage
  module Rack
    module App
      def self.app
        ::Rack::Builder.app do
          use ::Rack::RequestID # makes sure `env` has an X-Request-Id header

          use Triage::Rack::AttachLogger, Triage::Logging.rack_logger
          use Triage::Rack::AttachRequestId # Replaces logger with a child logger that's tagged with the request ID
          use Triage::Rack::RequestLogger # logs every request with timing data, request result, etc.

          use ::Rack::ContentType, 'application/json'
          use Triage::Rack::ErrorHandler

          map '/dashboard' do
            use ::Rack::Auth::Basic do |*credentials|
              Triage::Rack::Authenticator.secure_compare(
                credentials.join,
                ENV['GITLAB_DASHBOARD_TOKEN'].to_s)
            end

            run Triage::Rack::Dashboard.new
          end

          use Triage::Rack::Authenticator
          use Triage::Rack::WebhookEvent
          run Triage::Rack::Processor.new
        end
      end
    end
  end
end
