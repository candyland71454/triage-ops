# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class RemindMergedMrDeviatingFromGuideline < Processor
    HOURS_THRESHOLD_FOR_STALE_MR_PIPELINE = 2
    MERGE_RESULTS_REF_REGEX = %r{\Arefs/merge\-requests/\d+/merge\z}

    react_to 'merge_request.merge'

    def applicable?
      event.from_gitlab_org_gitlab? &&
        !event.target_branch_is_stable_branch? && # stable branch is moving slowly and the latest pipeline might not be a merged result one
        !latest_pipeline_valid?
    end

    def process
      remind_merged_mr_deviating_from_guidelines
    end

    def documentation
      <<~TEXT
        For merged MR, if the latest pipeline doesn't match these conditions:
          1. Pipeline is a canonical one
          2. Pipeline finished less than 2 hours ago
        We remind the MR merger about the "Merging a merge request" process.
      TEXT
    end

    private

    def project_id
      event.project_id
    end

    def latest_pipeline
      @latest_pipeline ||= Triage.api_client.merge_request_pipelines(project_id, event.iid).first
    end

    def latest_pipeline_valid?
      latest_pipeline_is_canonical? &&
        latest_pipeline_is_merge_results? &&
        latest_pipeline_is_recent_enough?
    end

    def latest_pipeline_is_canonical?
      event.with_project_id?(latest_pipeline&.project_id)
    end

    def latest_pipeline_is_merge_results?
      MERGE_RESULTS_REF_REGEX.match?(latest_pipeline&.ref)
    end

    def latest_pipeline_detailed
      @latest_pipeline_detailed ||= Triage.api_client.pipeline(project_id, latest_pipeline&.id)
    end

    def latest_pipeline_is_recent_enough?
      return true if latest_pipeline_detailed.finished_at.nil? # pipeline is still running

      Time.parse(latest_pipeline_detailed.finished_at) >= (Time.now - (3600 * HOURS_THRESHOLD_FOR_STALE_MR_PIPELINE))
    end

    def remind_merged_mr_deviating_from_guidelines
      comment = <<~MARKDOWN.chomp
        @#{event.event_actor_username}, did you forget to run a pipeline before you merged this work? Based on our [code review process](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request), if the latest pipeline finished more than 2 hours ago, you should:

        1. Ensure the merge request is not in Draft status.
        1. Start a pipeline (especially important for ~"Community contribution" merge requests).
        1. Set the merge request to merge when pipeline succeeds.

        This is a guideline, not a rule. Please consider replying to this comment for transparency.
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
