# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class LabelInference < Processor
    Label = Struct.new(:text, :raw_multiple_parents) do
      alias_method :to_s, :text

      def multiple_parents
        self[:raw_multiple_parents] || []
      end

      def has_multiple_parents?
        multiple_parents.any?
      end

      def ==(other_label)
        self.text == other_label.to_s
      end

      def scoped?
        text.end_with?('::')
      end

      def parent_of?(other_label)
        scoped? && other_label.to_s.start_with?(text)
      end

      def match?(other_label)
        parent_of?(other_label) || self == other_label
      end

      def multiple_parents_exclude?(labels)
        (multiple_parents & labels.map(&:text)).empty?
      end

      def to_markdown
        %Q(~"#{text}")
      end
    end

    LabelTree = Struct.new(:parent_label, :child_labels, :label_group) do
      def same_parent?(other_label_tree)
        parent_label.match?(other_label_tree.parent_label)
      end

      def same_label_group?(other_label_tree)
        return false if label_group.nil?

        other_label_tree.label_group == label_group
      end

      def child_label_name?(label_name)
        child_labels.any? { |label| label_name.include?(label.to_s) }
      end
    end

    LabelMatch = Struct.new(:label, :label_tree)

    WORK_TYPE_LABEL_TREES = [
      LabelTree.new(Label.new('type::feature'), [
        Label.new('feature::')
      ],
      :work_type),
      LabelTree.new(Label.new('type::maintenance'), [
        Label.new('maintenance::'),
      ],
      :work_type),
      LabelTree.new(Label.new('type::bug'), [
        Label.new('bug::'),
        Label.new('availability::limit')
      ],
      :work_type)
    ].freeze

    LABELS_TAXONOMY = [
      *WORK_TYPE_LABEL_TREES,
      LabelTree.new(Label.new('Engineering Productivity'), [
        Label.new('ep::')
      ]),
      LabelTree.new(Label.new('Engineering Allocation'), [
        Label.new('Eng-Consumer::'),
        Label.new('Eng-Producer::')
      ]),
      LabelTree.new(Label.new('bug::availability'), [
        Label.new('availability::limit')
      ])
    ].freeze

    react_to 'issue.*', 'merge_request.*'

    def applicable?
      (event.from_gitlab_org? || event.from_runbooks?) &&
        (added_child_labels_missing_parent? || work_type_changed_with_conflicting_subtypes?)
    end

    def process
      update_labels
    end

    def documentation
      <<~TEXT
        Adds/removes parent/child labels, whenever a new child label is added.
      TEXT
    end

    private

    def added_child_labels_missing_parent?
      child_labels_added? && parent_labels_to_add?
    end

    def work_type_changed_with_conflicting_subtypes?
      (work_type_parent_label_added? || work_type_parent_label_removed?) && child_labels_to_remove.any?
    end

    def work_type_parent_label_added?
      event.added_label_names.any? do |label_name|
        work_type_label_name?(label_name)
      end
    end

    def work_type_parent_label_removed?
      event.removed_label_names.any? do |label_name|
        work_type_label_name?(label_name)
      end
    end

    def work_type_label_name?(label_name)
      label_name.include?('type::')
    end

    def no_more_work_type_labels?
      event.label_names.none? { |label_name| work_type_label_name?(label_name) }
    end

    def child_labels_added?
      added_child_label_matches.any?
    end

    def parent_labels_to_add?
      current_parent_labels.empty? || parent_label_needs_to_be_changed?
    end

    def parent_label_needs_to_be_changed?
      added_child_label_matches.none? { |added_child_label_match| added_child_label_match.label.has_multiple_parents? } &&
        parent_labels_to_add.any?
    end

    def current_parent_labels
      @current_parent_labels ||= LABELS_TAXONOMY.select do |label_tree|
        event.label_names.include?(label_tree.parent_label.to_s)
      end.map(&:parent_label)
    end

    def current_subtype_labels
      @current_subtype_labels ||= begin
        label_names = event.label_names.select do |label_name|
          subtype_label_name?(label_name)
        end

        label_names.map { |label_name| Label.new(label_name) }
      end
    end

    def subtype_label_name?(label_name)
      WORK_TYPE_LABEL_TREES.any? do |label_tree|
        label_tree.child_label_name?(label_name)
      end
    end

    def added_child_label_matches
      @added_child_label_matches ||=
        event.added_label_names.each_with_object([]) do |label, memo|
          LABELS_TAXONOMY.each do |label_tree|
            label_tree.child_labels.each do |child_label|
              # We only detect one label per label group
              next if memo.any? { |label_match| label_tree.same_label_group?(label_match.label_tree) }

              memo << LabelMatch.new(child_label, label_tree) if child_label.match?(label)
            end
          end
        end
    end

    def parent_labels_to_add
      @parent_labels_to_add ||= added_child_label_matches
        .select { |added_child_label_match| added_child_label_match.label.multiple_parents_exclude?(current_parent_labels) }
        .map { |label| label.label_tree.parent_label } - current_parent_labels
    end

    def parent_labels_to_remove
      @parent_labels_to_remove ||= label_trees_in_same_label_groups(added_child_label_matches).map(&:parent_label).select do |parent_label|
        event.label_names.include?(parent_label.text)
      end
    end

    def label_trees_in_same_label_groups(label_matches)
      label_matches.flat_map do |label_match|
        LABELS_TAXONOMY.select do |label_tree|
          label_match.label.multiple_parents_exclude?([label_tree.parent_label]) &&
            !label_match.label_tree.same_parent?(label_tree) && label_match.label_tree.same_label_group?(label_tree)
        end
      end
    end

    def child_labels_to_remove
      @child_labels_to_remove ||= begin
        candidate_labels = event.label_names.map { |label_name| Label.new(label_name) }
        label_prefixes   = label_trees_in_same_label_groups(added_child_label_matches).map(&:child_labels).flatten

        conflicting_child_labels = candidate_labels.select do |candidate_label|
          label_prefixes.any? { |label_prefix| label_prefix.match?(candidate_label) }
        end

        conflicting_child_labels + orphaned_subtype_labels_after_type_label_removed
      end
    end

    def orphaned_subtype_labels_after_type_label_removed
      current_subtype_labels.select do |subtype_label|
        subtype_label_orphaned?(subtype_label)
      end
    end

    def subtype_label_orphaned?(subtype_label)
      label_tree_for_subtype_label = WORK_TYPE_LABEL_TREES.find do |label_tree|
        label_tree.child_labels.any? do |child_label|
          child_label.match?(subtype_label)
        end
      end

      raise ArgumentError, "#{subtype_label} is not a valid subtype label!" if label_tree_for_subtype_label.nil?

      !current_parent_labels.include?(label_tree_for_subtype_label.parent_label) &&
        !parent_labels_to_add.include?(label_tree_for_subtype_label.parent_label)
    end

    def update_labels
      comment = ''

      # Add parent labels
      comment += "/label #{parent_labels_to_add.map(&:to_markdown).join(' ')}" if parent_labels_to_add.any?

      # Remove parent/child labels
      labels_to_remove = parent_labels_to_remove + child_labels_to_remove
      comment += "\n/unlabel #{labels_to_remove.map(&:to_markdown).join(' ')}" if labels_to_remove.any?

      add_comment(comment, append_source_link: false) unless comment.empty?
    end
  end
end
