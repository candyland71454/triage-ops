# frozen_string_literal: true

require_relative 'community_processor'
require_relative 'automated_review_request_generic'
require_relative '../../triage/documentation_code_owner'
require_relative '../../triage/changed_file_list'

module Triage
  class AutomatedReviewRequestDoc < CommunityProcessor
    DOC_FILE_REGEX = %r{\Adocs?/}.freeze

    react_to 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        !merge_request_labelled_technical_writing? &&
        workflow_ready_for_review_added? &&
        merge_request_changes_doc? &&
        unique_comment.no_previous_comment?
    end

    def process
      post_documentation_label_comment
    end

    def documentation
      <<~TEXT
        This processor identifies merge requests with documentation changes,
        labels them with ~"#{Labels::TECHNICAL_WRITING_LABEL}" and pings a technical writer to review.
      TEXT
    end

    private

    def merge_request_changes_doc?
      Triage::ChangedFileList.new(project_id, merge_request_iid).any_change?(DOC_FILE_REGEX)
    end

    def merge_request_labelled_technical_writing?
      event.label_names.any? do |label|
        label == Labels::TECHNICAL_WRITING_LABEL ||
        label.start_with?('tw::')
      end
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def post_documentation_label_comment
      comment = <<~MARKDOWN.chomp
        #{message if approvers.any?}
        /label ~"#{Labels::DOCUMENTATION_LABEL}" ~"#{Labels::TECHNICAL_WRITING_TRIAGED_LABEL}"
      MARKDOWN

      add_comment(comment.strip, append_source_link: approvers.any?)
    end

    def message
      random_approver = approvers.sample(random: Random.new(OpenSSL::Digest::SHA256.hexdigest(event.source_branch).to_i(16)))
      comment = <<~MESSAGE.chomp
        Hi `@#{random_approver}`! Please review this ~"#{Labels::DOCUMENTATION_LABEL}" merge request.
        /assign_reviewer @#{random_approver}
      MESSAGE

      unique_comment.wrap(comment)
    end

    def approvers
      @approvers ||=
        Triage::DocumentationCodeOwner
          .new(project_id, merge_request_iid)
          .approvers
    end
  end
end
