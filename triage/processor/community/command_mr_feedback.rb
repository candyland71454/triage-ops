# frozen_string_literal: true

require 'digest'
require 'slack-messenger'

require_relative 'community_processor'

require_relative '../../triage'
require_relative '../../triage/rate_limit'

module Triage
  class CommandMrFeedback < CommunityProcessor
    include RateLimit

    SLACK_CHANNEL = '#mr-feedback'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hola MR coaches, `@%<username>s` has left feedback about their experience in %<comment_url>s.
    MESSAGE

    react_to 'merge_request.note'
    define_command name: 'feedback'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      valid_command?
    end

    def process
      post_mr_feedback_request
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    def documentation
      <<~TEXT
        This processor pings the #mr-coaching channel when community contributors provided code review feedback.
      TEXT
    end

    private

    attr_reader :messenger

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("feedback-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end

    def post_mr_feedback_request
      message = format(SLACK_MESSAGE_TEMPLATE, comment_url: event.url, username: event.noteable_author.username)
      note = {
        text: get_note_text(event)
      }

      messenger.ping(text: message, attachments: [note])
    end

    def slack_messenger
      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], slack_options)
    end

    def get_note_text(event)
      event.new_comment.sub("#{GITLAB_BOT} feedback ", "")
    end
  end
end
