# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../../lib/team_member_select_helper'

module Triage
  class AutomatedReviewRequestGeneric < CommunityProcessor
    include TeamMemberSelectHelper

    GROUP_LABEL_PREFIX = 'group::'
    GROUP_LABEL_REGEX = /^#{GROUP_LABEL_PREFIX}(?<group_name>.*)$/.freeze

    react_to 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        workflow_ready_for_review_added? &&
        not_a_distribution_project?
    end

    def process
      post_review_request_comment
    end

    private

    # The Distribution team has their own review workflow so we don't assign until we're sure they're ok with this new workflow
    # See https://about.gitlab.com/handbook/engineering/development/enablement/distribution/merge_requests.html#workflow
    def not_a_distribution_project?
      !WwwGitLabCom.distribution_projects.include?(event.project_id)
    end

    def post_review_request_comment
      comment = <<~MARKDOWN.strip
      #{intro_sentence}

      - Do you have capacity and domain expertise to review this? We are mindful of your time, so if you are not
        able to take this on, please re-assign to one or more other reviewers.
      - Add the ~"#{Labels::WORKFLOW_IN_DEV_LABEL}" label if the merge request needs action from the author.
      /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end

    def intro_sentence
      if current_reviewers.any?
        <<~MARKDOWN.strip
        #{current_reviewers.join(' ')}, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review.
        MARKDOWN
      else
        <<~MARKDOWN.strip
        `#{coach}`, this ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" is ready for review.

        /assign_reviewer #{coach}
        MARKDOWN
      end
    end

    def current_reviewers
      @current_reviewers ||= merge_request.reviewers.map { |reviewer| "`@#{reviewer['username']}`" }
    end

    def merge_request
      @merge_request ||= Triage.api_client.merge_request(event.project_id, event.iid)
    end

    def coach
      @coach ||= select_random_merge_request_coach(group: group)
    end

    def group
      event.label_names.grep(GROUP_LABEL_REGEX).first&.delete_prefix(GROUP_LABEL_PREFIX)
    end
  end
end
