# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class RemindAuthorNotCompliantToMergeMr < Processor
    react_to 'merge_request.merge'

    def applicable?
      event.from_gitlab_org_gitlab? && event.by_resource_author?
    end

    def process
      remind_author_not_compliant_to_merge_mr
    end

    def documentation
      <<~TEXT
        This processor reacts to the MR merge actions done by the MR author
        by posting a reminder comment that it is a non-compliant action.
      TEXT
    end

    private

    def remind_author_not_compliant_to_merge_mr
      comment = <<~MARKDOWN.chomp
        Hi @#{event.event_actor_username},

        Please note that authors are not authorized to merge their own merge requests and need to seek another maintainer to merge.

        For more information please refer to:
        - [GitLab Security Compliance Controls](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
        - [Change Management Controls CMG-04](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/change-management.html)
        - [Feedback issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/43)

        /label ~"self-merge"
      MARKDOWN

      add_comment(comment, append_source_link: true)
    end
  end
end
