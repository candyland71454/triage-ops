# frozen_string_literal: true

require_relative '../triage/changed_file_list'
require_relative '../triage/change/new_rubocop'
require_relative '../triage/processor'

module Triage
  class NotifyChanges < Processor
    react_to \
      'merge_request.open',
      'merge_request.update', # Notify that changes we might merge
      'merge_request.merge'   # Notify that it is merged

    GITLAB_HANDLE_TO_PING = 'godfat-gitlab'
    COLLECTING_ISSUE = '/projects/23809675/issues/254' # https://gitlab.com/gitlab-jh/gitlab-jh-enablement/-/issues/254
    INTERESTED_CHANGES = [
      NewRuboCop
    ].freeze

    def applicable?
      event.from_gitlab_org_gitlab? &&
        not_notified_already? &&
        changes_should_notify.any?
    end

    def process
      message =
        if event.merge_event?
          notification_when_merged
        else
          notification_upon_detection
        end

      if discussion_thread
        append_discussion(message, discussion_thread['id'], append_source_link: true)
      else
        add_discussion(message, append_source_link: true)
        resolve_discussion(discussion_thread['id'])
      end

      # Avoid double posting
      post_to_collecting_issue unless event.merge_event?
    end

    def documentation
      <<~MARKDOWN
        Notify JiHu about changes that might break JiHu
      MARKDOWN
    end

    private

    def not_notified_already?
      # Always notify for merge event, which should happen only once
      event.merge_event? || unique_comment.no_previous_comment?
    end

    def changes_should_notify
      @changes_should_notify ||= INTERESTED_CHANGES.flat_map do |change|
        change.detect(changed_file_list)
      end
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def notification_when_merged
      notification do
        <<~MARKDOWN
          This merge request is merged with the following changes that might break JiHu:

          #{file_list_to_notify}

          /cc @#{GITLAB_HANDLE_TO_PING}
        MARKDOWN
      end
    end

    def notification_upon_detection
      notification do
        <<~MARKDOWN
          Changes detected for the following files that might break JiHu:

          #{file_list_to_notify}

          /cc @#{GITLAB_HANDLE_TO_PING}
        MARKDOWN
      end
    end

    def notification
      unique_comment.wrap(yield.strip)
    end

    def file_list_to_notify
      changes_should_notify.map do |change|
        "- `#{change.path}` (#{change.type})"
      end.join("\n")
    end

    def discussion_thread
      unique_comment.previous_discussion
    end

    def post_to_collecting_issue
      Reaction.add_comment(<<~MARKDOWN.strip, COLLECTING_ISSUE, append_source_link: false)
        Changes detected from this merge request: #{event.url}
      MARKDOWN
    end
  end
end
