# frozen_string_literal: true

source "https://rubygems.org"

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

gem "json", "~> 2.6.2"
gem "activesupport", "~> 7.0.4"
gem "sentry-raven", require: false
gem "gitlab-triage", "~> 1.24.0"
gem "gitlab", "~> 4.19.0", require: false

# Picked because it's used in gitlab. Can use any which follows redirect
gem "httparty"

# The `1.13.6` release queries fields that aren't present in GitLab.com's current schema.
# See https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/876#note_822424264 and https://github.com/rmosolgo/graphql-ruby/pull/3854.
gem "graphql", "1.13.5"

# Reactive
gem "mini_cache", "~> 1.1.0", require: false
gem "sucker_punch", "~> 3.1.0", require: false
gem "rack", require: false
gem "rack-requestid", "~> 0.2", require: false
gem "puma", require: false
gem "slack-messenger", require: false
gem "ougai", "~> 2.0.0", require: false

group :development, :test do
  gem 'gitlab-styles', '~> 9.0.0', require: false
end

group :development do
  gem "guard-rspec"
  gem "guard"
  gem "lefthook", require: false
  gem "pry-byebug", "~> 3.9.0"
end

group :test do
  gem "rspec", require: false
  gem "rspec-parameterized", require: false
  gem "rspec_junit_formatter", require: false
  gem "rack-test", require: false
  gem "timecop", require: false
  gem "webmock", require: false
end

group :danger, optional: true do
  gem 'gitlab-dangerfiles', '~> 3.5.2', require: false
end

group :coverage, optional: true do
  gem 'simplecov', '~> 0.21.2', require: false
  gem 'simplecov-lcov', '~> 0.8.0', require: false
  gem 'simplecov-cobertura', '~> 2.1.0', require: false
  gem 'undercover', '~> 0.4.5', require: false
end
